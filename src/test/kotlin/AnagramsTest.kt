import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

class AnagramsTest {
    @Test
    fun Test() {
        assertEquals(Anagrams.areAnagrams("pooya","pooya"),false)
        assertEquals(Anagrams.areAnagrams("pooya"," pooya"),false)
        assertEquals(Anagrams.areAnagrams("pooya"," p o o y a"),false)
        assertEquals(Anagrams.areAnagrams("POOYA","ayoop"),true)
        assertEquals(Anagrams.areAnagrams("رافص","صراف"),true)
        assertEquals(Anagrams.areAnagrams("evil","vile"),true)
        assertEquals(Anagrams.areAnagrams("a gentleman","elegant man"),true)
        assertEquals(Anagrams.areAnagrams("forty five","over fifty"),true)
        assertEquals(Anagrams.areAnagrams("ae","bd"),false)

        assertEquals(Anagrams.areAnagramsPro("pooya","pooya"),false)
        assertEquals(Anagrams.areAnagramsPro("pooya"," pooya"),false)
        assertEquals(Anagrams.areAnagramsPro("pooya"," p o o y a"),false)
        assertEquals(Anagrams.areAnagramsPro("POOYA","ayoop"),true)
        assertEquals(Anagrams.areAnagramsPro("رافص","صراف"),true)
        assertEquals(Anagrams.areAnagramsPro("evil","vile"),true)
        assertEquals(Anagrams.areAnagramsPro("a gentleman","elegant man"),true)
        assertEquals(Anagrams.areAnagramsPro("forty five","over fifty"),true)
        assertEquals(Anagrams.areAnagramsPro("ae","bd"),false)
    }

    @OptIn(ExperimentalTime::class)
    @Test
    fun performanceTest(){
        /*
        * comparing simple method's performance with pro method
        */
        val totalTime = measureTime {
            for (i in 1..10000){
                Anagrams.areAnagrams("forty five","over fifty")
            }
        }

        val totalTimePro = measureTime {
            for (i in 1..10000){
                Anagrams.areAnagramsPro("forty five","over fifty")
            }
        }
        println("[areAnagrams] total time is: $totalTime")
        println("[areAnagramsPro] total time is: $totalTimePro")

        // AnagramPro performance should be better
        assertEquals(totalTimePro < totalTime , true)
    }

}