/**
 * Created by Pooya Sarraf on 7/20/2023.
 */

fun main(args: Array<String>) {
    println("AnagramsPro is started...")
    println("==========================")
    checkAnagrams()
}

fun checkAnagrams(){
    var shouldRepeat = true

    // reading console input
    while (shouldRepeat){

        println("Please enter the first word:")
        val t1 = readln()
        println("Please enter the second word:")
        val t2 = readln()

        if (t1.isNotEmpty() && t2.isNotEmpty()){

            println("You have entered: $t1 & $t2")
            if (Anagrams.areAnagramsPro(t1,t2))
                println("✅ They are anagrams")
            else
                println("❌ They are not anagrams")

            shouldRepeat = false
        } else {
            println("⚠️ Invalid input. Please try again.")
        }
    }
}