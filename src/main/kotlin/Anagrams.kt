class Anagrams {

    companion object{
        /**
         * Returns true if text1 and text2 are anagrams
         */
        fun areAnagramsPro(text1:String,text2:String):Boolean{

            val t1 = text1.trim().lowercase()
            var t2 = text2.trim().lowercase()

            // equal strings are not anagrams
            if (t1 == t2)
                return false

            // no further check is required if texts length are different
            if (t1.length != t2.length)
                return  false

            // replacing chars one by one.
            // if no characters left it means they are anagrams
            t1.forEach {
                t2 = t2.replaceFirst(it.toString(),"")
            }

            if (t2.length == 0)
                return true
            else
                return false
        }

        /**
         * Returns true if text1 and text2 are anagrams
         */
        fun areAnagrams(text1:String,text2:String):Boolean{

            val t1 = text1.trim().lowercase()
            val t2 = text2.trim().lowercase()

            // equal strings are not anagrams
            if (t1 == t2)
                return false

            // no further check is required if texts length are different
            if (t1.length != t2.length)
                return  false

            // anagrams text should be equal after sorting alphabetically
            val sortedT1 = t1.split("").sorted()
            val sortedT2 = t2.split("").sorted()

            if (sortedT1 == sortedT2)
                return true
            else
                return false
        }
    }
}