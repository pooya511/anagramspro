# Anagrams Pro
----

 A simple Koltin project to check if two inputs are anagrams.

- [Anagrams Wiki](https://en.wikipedia.org/wiki/Anagram) - Read more about anagrams at Wikipedia.

## Usage guide
```kotlin
    val t1 = "sample"
    val t2 = "plesam"
    println(Anagrams.areAnagramsPro(t1,t2))

    //output: true

    val t3 = "example"
    val t4 = "xampl"
    println(Anagrams.areAnagramsPro(t3,t4))

    //output: false
```